
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
{

	userId: { type: String },
	
	products: [{
		productId: {type: String},
		productName: {type: String},
		quantity: {
			type: Number, 
			default: 1
			},
		total: {type: Number}
	}],
	totalAmount: {type: Number},
	purchasedOn: {
		type: Date, 
		default: new Date()
		},
	isActive : {
		type : String,
		default : "Pending"
	}
});

module.exports = mongoose.model("Order", orderSchema);



// backup
// //////
// ORder Model
// const mongoose = require("mongoose");

// const orderSchema = new mongoose.Schema(
// {

// 	userId: { type: String },
	
// 	products: [{
// 		productId: {type: String},
// 		productName: {type: String},
// 		quantity: {
// 			type: Number, 
// 			default: 1
// 			},
// 		total: {type: Number}
// 	}],
// 	totalAmount: {type: Number},
// 	purchasedOn: {
// 		type: Date, 
// 		default: new Date()
// 		},
// 	isActive : {
// 		type : Boolean,
// 		default : true
// 	}
// });

// module.exports = mongoose.model("Order", orderSchema);


// Order Controller

// const Order = require("../models/Order");
// //Require to access the models/Product.js file
// const Product = require("../models/Product");
// //Require to access the models/User.js file

// const User = require("../models/User");

// // "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
// const bcryptjs = require("bcryptjs");

// const auth = require("../auth");



// // Non-admin user checkout
// module.exports.checkOut = async (data) => {
//   if (data.isAdmin) {
//     return 'Admin is not allowedto checkOut';
//   }

//   const products = data.body.products || [];
//   const order = new Order({
//     userId: data.userId,
//     products: []
//   });

//   let totalAmount = 0;

//   for (const product of products) {
//     const findProd = await Product.findById(product.productId);
//     const addProd = {
//       productId: findProd._id,
//       productName: findProd.productName,
//       quantity: product.quantity,
//       total: product.quantity * findProd.price
//     };
//     totalAmount += addProd.total; 	
//     order.products.push(addProd);
//   }

//   order.totalAmount = totalAmount
//   return order.save().then((savedOrder) => savedOrder);
// };


// // Retrieve all orders
// module.exports.getAllOrder = () => {
// 	return Order.find({}).then(result => {
// 		return result;
// 	});
// };


// // // Archiving an order
// // module.exports.archiveOrder = (reqParams) => {
// // 	let updateActiveField = {
// // 		isActive: false
// // 	};
// // 	return Order.findByIdAndUpdate(reqParams.orderId, updateActiveField).then((order, error) => {
// // 		if (error) {
// // 			return false;
// // 		} else {
// // 			return true;
// // 		};
// // 	});
// // };

// Order Route

// const express = require("express");

// // Creates a router instance that functions as a middleware and routing system
// const router = express.Router();

// // Route for checking if the user's email already exists in the database
// const orderController = require("../controllers/orderController");

// const auth = require("../auth");




// // Route for checking out order
// router.post("/checkOut", auth.verify, (req, res) => {
// 	const data = {
// 	userId: auth.decode(req.headers.authorization).id,
// 	isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 	body: req.body
// 	}

// 	orderController.checkOut(data).then(resultFromController => res.send(resultFromController));
// });


// // Route for retrieving all the orders
// router.get("/getAllOrder" , (req, res) => {
// 	orderController.getAllOrder().then(resultFromController => res.send(resultFromController));
// });


// // Route to archiving a order

// router.patch("/:orderId/archive", auth.verify, (req, res) => {
// 	orderController.archiveOrder(req.params).then(resultFromController => res.send(resultFromController));
// });


// ///////////////

// // // Route for retrieving user details

// // router.post("/userDetails", auth.verify, (req, res) => {

// // 	const userData = auth.decode(req.headers.authorization);

// // // Provides the user's ID for the getProfile controller method
// // userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
// // });





















// router.post("/order", auth.verify, (req,res) =>{
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.productId
// 	}
// 	orderController.order(data).then(resultFromController =>
// 	res.send(resultFromController));
// });






// // Allows us to export the "router" object that will be accessed in our "index.js" file
// module.exports = router;









