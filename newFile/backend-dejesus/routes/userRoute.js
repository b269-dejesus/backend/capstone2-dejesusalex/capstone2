const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
const userController = require("../controllers/userController");

const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details

// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a product
router.post("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

// Provides the user's ID for the getProfile controller method
userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for authenticated user enrollment
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		// User ID will be retrieved from the request header
		userId: auth.decode(req.headers.authorization).id,
		// Product ID will be retrieved from the request body
		productId: req.body.productId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});







// Route for retrieving a user's orders
router.post("/:id/orders", auth.verify, (req, res) =>
{
	const userData = auth.decode(req.headers.authorization)
	userController.getUserOrders(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for adding to cart and updating cart(Non-Admin)
router.put("/:id/cart/add", auth.verify, (req,res) =>
{
	let data =
	{
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body //productId and quantity
	}
	userController.addToCart(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for removing item from cart (Non-Admin)
router.put("/:id/cart/remove", auth.verify, (req,res) =>
{
	let data =
	{
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body //productId
	}
	userController.removeFromCart(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for accessing user cart
router.post("/:id/cart", auth.verify, (req,res) =>
{
	const userData = auth.decode(req.headers.authorization);

	userController.getCart(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for checking out all items in user's cart
router.post("/:id/cart/checkout", auth.verify, (req,res) =>
{
	const userData = auth.decode(req.headers.authorization);

	userController.checkOutCart(req.params, userData).then(resultFromController => res.send(resultFromController));
});




// Route for retrieving all the users
router.get("/all" , (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
// router.get("/details", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
// });


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;