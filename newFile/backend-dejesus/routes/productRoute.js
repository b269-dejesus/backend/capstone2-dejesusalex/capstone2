const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");



router.post("/create", (req, res) => {

	//  	const data = {
	// 	product: req.body,
		// isAdmin: auth.decode(req.headers.authorization).isAdmin
	// }
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products
router.get("/all" , (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving active products
router.get("/active" , (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});



// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a product
router.put("/:productId/update", (req,res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// S40 Activity
// Route to archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});
// S40 Activity

module.exports = router;


// Route to reactivating a product
router.put("/:productId/activate", (req,res) => {
	productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route to deactivating a product
router.put("/:productId/deActivate", (req,res) => {
	productController.deActivateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for buying product immediately from page without adding to cart (Non-Admin)
router.post("/:productId/buynow", auth.verify, (req,res) =>
{
	let data =
	{
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body //quantity
	}
	productController.buyNow(req.params, data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
