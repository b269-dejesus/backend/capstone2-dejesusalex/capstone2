const Cart = require("../models/Cart");
const mongoose = require("mongoose");
const Product = require("../models/Product");
const Order = require("../models/Order");



// Adding products to the cart / update quantity
module.exports.addToCart = async (data) => {
  // if auth key is admin, dont proceed
  if (data.isAdmin) {
    return 'User must NOT be an admin to access this!';
  }


  // check user cart if existing
  let cart = await Cart.findOne({userId: data.userId});


  // if cart is not existing, create new cart
  if (!cart){
    const cart = new Cart({
      userId: data.userId,
      products: [],
      totalPrice: 0
      
    });

    // Initialize totalAmount to 0
    let totalAmount = 0;
    
      // find the information of the product by its id
      const foundProduct = await Product.findById(data.body.productId);

      // initialize fields connected to Product collection
      const addCart = {
        productId: foundProduct._id,
        productName: foundProduct.name,
        quantity: data.body.quantity,
        price: foundProduct.price,
        subTotal: data.body.quantity * foundProduct.price,
        img: foundProduct.img
      }
      // addition assignment to totalAmount by each product's subtotal
      totalAmount += addCart.subTotal
      // push each product to the products array
      cart.products.push(addCart);
    

    // set totalPrice to the summation of all products
    cart.totalPrice += totalAmount;
    // save cart information
    return cart.save().then((savedCart) => {if(savedCart){
      return true;
      }else{
        return false;
      }})
  

}
  // else if the cart is existing, update the cart

      const foundProduct = await Product.findById(data.body.productId);

      // check if product is existing in the cart compare strings
      const existingProduct = cart.products.find(
        (p) => p.productId.toString() === foundProduct._id.toString()
      );

      // if existing, update the product
      if (existingProduct) {
        existingProduct.quantity += data.body.quantity;
        existingProduct.subTotal = existingProduct.quantity * foundProduct.price;
      } else {
        // if product is not existing, add new product to the array
        const addCart = {
          productId: foundProduct._id,
          productName: foundProduct.name,
          quantity: data.body.quantity,
          price: foundProduct.price,
          subTotal: data.body.quantity * foundProduct.price,
          img: foundProduct.img
        }
        cart.products.push(addCart);
      }
    

    // calculate the total price of all the products by iterating over cart.products
    cart.totalPrice = cart.products.reduce(
      (total, product) => total + product.subTotal,
      0
    );
    // save the cart new cart/update
    return cart.save().then((savedCart) => {
      if(savedCart){
        return true
      }else{
        return false
      }
    });
}




// remove a product from the cart
module.exports.removeFromCart = async (data) => {
  // check if auth key is admin, don't proceed if true
  if (data.isAdmin) {
    return false;
  }

  // if auth key is not admin, find existing cart by userId
  let cart = await Cart.findOne({ userId: data.userId });

  // if no cart is existing, return message and don't proceed
  if (!cart) {
    return undefined;
  }

  // find the index of the product to remove in the products array
  const productIndex = cart.products.findIndex(
    (product) => product.productId === data.body.productId
  );

  // if product is found in the products array, remove it
  if (productIndex !== -1) {
    cart.products.splice(productIndex, 1);
    // calculate the total price after removing the product
    cart.totalPrice = cart.products.reduce(
      (total, product) => total + product.subTotal,
      0
    );
    // save cart changes
    return cart.save().then((updatedCart) => {
      if (updatedCart) {
        return true;
      } else {
        return false;
      }
    });
  } else {
    return false;
  }
};



// Create an order using the cart model
module.exports.checkOut = async (data) => {
  // if auth key is admin, dont proceed
  if (data.isAdmin) {
      return 'User must NOT be an admin to access this!';
    }

    // find cart with userId
    return await Cart.findOne({userId: data.userId}).then(result => {

      // if cart is not existing, dont proceed
      if(!result){
        return 'User cart not found'
      } 

      // else, create new Order with found cart
      const cartOut = new Order({
        userId: data.userId,
        products: result.products,
        totalAmount: result.totalPrice
      })
      // save new Order
      return cartOut.save().then((save) => {
            return save;
          }).catch((err) => {
            return err;
          });
    })
}

// show user's cart
module.exports.showCart = (data) => {
  // if decoded auth key isAdmin is true, dont proceed.
  if (data.isAdmin) {
    return Promise.resolve(false); // Return a resolved Promise with value false
  } else {
    // else if isAdmin is false, find one using userId and return result
    return Cart.findOne({userId: data.userId}).exec() // Call .exec() to return a Promise
      .then((result) => {
        if (result) {
          return result;
        } else {
          return Promise.resolve(false); // Return a resolved Promise with value false
        }
      })
      .catch((err) => {
        console.error(err);
        return Promise.resolve(false); // Return a resolved Promise with value false
      });
  }
};

// remove all products from cart
module.exports.removeAll = async (data) => {

  // if decoded auth key isAdmin is true, dont proceed.
  if (data.isAdmin) {
      return 'User must NOT be an admin to access this!';
    }

    // if auth key is not admin, find existing cart by userId
    let cart = await Cart.findOne({userId: data.userId});

    // if no cart is existing, return message and dont proceed
    if(!cart) {
      return "The user's cart is empty, please add a product first!"
    }

    // if cart is existing, remove all product in products array using the $pull method.
    const newCart = await Cart.findOneAndUpdate(
      {userId: data.userId}, 
      {$pull: {products: {}},$set: { totalPrice: 0 } },
      {multi: true}
      );

    // save cart changes
    return newCart.save().then((update, err) => {
      if(err){
        return err;
      }else{
        return true;
      }
    });

}
