const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {type: String},
	products: [{
		productId: {type: String},
		productName: {type: String},
		quantity: {type: Number, default: 1},
		price: {type: Number},
		subTotal: {type: Number},
		img: {type: String}
	}],
	totalPrice: {type: Number, default: 0}
})

module.exports = mongoose.model("Cart", cartSchema);