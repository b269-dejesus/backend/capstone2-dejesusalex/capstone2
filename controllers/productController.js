const Product = require("../models/Product");



module.exports.addProduct = (reqBody) => {
	// User is an admin
	// if (data.isAdmin) {
		// Creates a variable "newProducts" and instantiates a new "Product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
			stock : reqBody.stock
		});
		// Saves the created object to our database
		return newProduct.save().then((products, error) => {
			// Product creation successful
			if (error) {
				return false;
			// Product creation failed
			} else {
				return true;
			};
		});
	// };
	// User is not an admin
// 	let message = Promise.resolve('User must be ADMIN to access this!');
// 	return message.then((value) => {
// 		return {value};
// 	});
};


// Retrieve ALL Productss
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Retrieve ACTIVE Productss
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};


// Retrieve specific Products
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update a Products
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};

// S40 Activity
// Archive a Products
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
// S40 Activity End

// Activate Product

module.exports.activateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		isActive: true
 		
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


// deactivate product
module.exports.deActivateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		isActive: false
 		
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


// Controller function for directly checking out a product without adding to cart as non-admin user(basic capstone requirement)
module.exports.buyNow = async (reqParams, data) =>
{
	try
	{
		// checking if user is non-admin
		if(!data.isAdmin)
		{
			// Creating new order
			const savedOrder = await Product.findById(reqParams.productId).then(product =>
			{
				// checking if the available stock is enough for the quantity
				let quantity = 1

				if(data.reqBody.quantity)
				{
					quantity = data.reqBody.quantity
				}
				if(quantity <= product.stock)
				{
					// updating product information
					updatedStock = product.stock - quantity;
					updatedCount = product.buyCount + quantity;
					
					Product.findByIdAndUpdate(product._id,{stock: updatedStock, buyCount: updatedCount}).then(product =>
					{
						return product.save()
					})

					let computedSubTotal = product.finalPrice * quantity;
					const newOrder = new Order
					({
						userId: data.userId,
						// takes a screenshot of the products relevant information at the time of purchase
						products: 
						[{
							productId: reqParams.productId,
							name: product.name,
							description: product.description,
							price: product.price,
							salePercent: product.salePercent,
							quantity: quantity,
							finalPrice: product.finalPrice,
							subTotal: computedSubTotal
						}],
						// NOTE: the "Buy Now" feature instantly checks out one product only, therefore the totalAmount will always be equal to the subTotal. This is not the case for checking out from a user's cart and the totalAmount computation is applied accordingly.
						totalAmount: computedSubTotal
					});

					return newOrder.save().then((order,error) =>
						{
							if (error) {return false;}
							else {return order;}
						});
				}
				else
				{
					throw new Error("Cannot order more than available stock.");
				}
			})
			
			// Updating user's order
			const updateUser = await User.findById(data.userId).then(user =>
			{
				user.orders.push({orderId: savedOrder._id});
				return user.save().then((user,error) =>
				{
					if (error) {return false;}
					else {return user;}
				});
			});

			if(savedOrder !== false && updateUser !== false)
			{
				return ({"success": `Order successfully placed.`, order: savedOrder});
			}
			else
			{
				return ({"error":"Something went wrong."});
			}
		}
		else
		{
			return ({"error":"Admin users are not authorized to check out products."});
		}
	}
	catch(error)
	{
		return ({"error":error.message});
	}
};
