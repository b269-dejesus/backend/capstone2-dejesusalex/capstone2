// importing from packages
import { useState,useEffect,useContext } from 'react';

// importing from within the app
import UserContext from "../UserContext"
import OrderCard from '../components/OrderCard';

export default function Orders()
{
	const {user} = useContext(UserContext);
	const [orders,setOrders] = useState();

	useEffect(() =>
	{
		fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/orders`,
		{
			method: "POST",
			headers:
			{
			  Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
			.then(res => res.json()).then(data =>
			{
				console.log(data)
				setOrders(data.map(order =>
				{
					return (<OrderCard key={order._id} order={order}/>)
				}))
			})
	},[user.id])

	/*return(
	<div>
	  {orders.map((order) => (
	    <OrderCard key={order._id} order={order} />
	  ))}
	</div>
	)*/

	return(
		<>
		{orders}
		</>
	);
} 
