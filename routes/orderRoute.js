const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
const orderController = require("../controllers/orderController");

const auth = require("../auth");




// Route for checking out order
router.post("/checkOut", auth.verify, (req, res) => {
	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	body: req.body
	}

	orderController.checkOut(data).then(resultFromController => res.send(resultFromController));
});




// Retrieve all orders (Admin only)
router.get("/admin/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve pending orders
router.get("/admin/pending", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderController.getPending(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve approved orders
router.get("/admin/approved", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderController.getApproved(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Delete an order by order id and userId 
router.delete("/cancel", auth.verify, (req, res) => {
	const data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin,
	body: req.body
	}

	orderController.deleteOrder(data).then(resultFromController => res.send(resultFromController));
})

// decline an order (admin)
router.delete("/admin/cancel", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orderId: req.body.orderId
	}

	orderController.declineOrder(data).then(resultFromController => res.send(resultFromController));
})

// Approve an order
router.post("/admin/approve", auth.verify, (req, res) => {
	const data ={ 
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orderId: req.body.orderId
	}

	orderController.approveOrder(data).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all the orders
router.get("/getAllOrder" , (req, res) => {
	orderController.getAllOrder().then(resultFromController => res.send(resultFromController));
});
// Route for retrieving all the orders
router.get("/getActiveOrder" , (req, res) => {
	orderController.getActiveOrders().then(resultFromController => res.send(resultFromController));
});



// Route to archiving a order

router.patch("/:orderId/archive", auth.verify, (req, res) => {
	orderController.archiveOrder(req.params).then(resultFromController => res.send(resultFromController));
});


///////////////

// // Route for retrieving user details

// router.post("/userDetails", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// // Provides the user's ID for the getProfile controller method
// userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
// });





















router.post("/order", auth.verify, (req,res) =>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	orderController.order(data).then(resultFromController =>
	res.send(resultFromController));
});






// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;

